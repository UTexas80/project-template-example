# Example Project

You can clone this project into your working repository and then update it. Or fork it.

This example includes:
- `project.tex`: the barebones of a project to showcase a standard project.
- `project.bib`: the barebones of a bibliography that goes with `project.tex`.
- `.gitlab-ci.yml`: an example of a pipeline to build projects using the [`project-template`](https://gitlab.com/adn-latex/project-template) and its dependencies automatically. 

## Dependencies

This project depends on 
- the bibstrings defined in [`git@gitlab.com:adin/journal-list.git`](https://gitlab.com/adin/journal-list).
- the project template defined in [`git@gitlab.com:adn-latex/project-template.git`](https://gitlab.com/adn-latex/project-template)
